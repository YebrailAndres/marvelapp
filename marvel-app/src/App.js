import React from "react";
import Header from "./Components/Header/Header";
import Layout from "./Components/Layout/Layout";

import Hero from "./Components/Hero/Hero";

import "./App.css";

const App = () => (
  <div>
    <Header />
    <Hero />
    <Layout />
  </div>
);

export default App;
