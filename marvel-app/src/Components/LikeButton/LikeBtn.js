import React, { Component } from "react";
import "./LikeBtn.css";

class LikeBtn extends Component {
  constructor() {
    super();
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.state = { value: 0, value2: 0 };
  }

  increment() {
    this.setState({ value: this.state.value + 1 });
  }
  decrement() {
    this.setState({ value2: this.state.value2 + 1 });
  }
  render() {
    return (
      <div>
        <a className="button heart" onClick={this.increment} />
        <span className="Likes">{`Likes: ${this.state.value}`}</span>
        <a className="button cross" onClick={this.decrement} />
        <span className="Dislikes">{`Dislikes: ${this.state.value2}`}</span>
      </div>
    );
  }
}

export default LikeBtn;
