import React from "react";
import './Header.css';

const Header = () => (
  <header id="header" className="alt">
    <div className="logo">
      <h3>Marvel</h3>
    </div>
    <a href="#menu">Menu</a>
  </header>
);

export default Header;
