import React, { Component } from "react";
import { getComics } from "../../utils/api";
import Comics from '../Comics/Comics';
import "./Layout.css";

class Layout extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.loadData();
  }

  async loadData() {
    try {
      const {
        data: {
          data: { results },
        }
      } = await getComics();
      this.setState({ results });
    } catch (error) {
    }
  }
  render() {
    return (
      <section id="one" className="wrapper style2">
        <div className="inner">
          <div className="grid-style">
            {this.state.results ? (
              this.state.results.map(post => (
                <Comics
                  image={post.thumbnail.path}
                  key={post.id}
                  title={post.title.split("(")[0]}
                  date={post.dates[0].date.split("T")[0]}
                />
              ))
            ) : (
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"
                alt="Images"
              />
            )}
          </div>
        </div>
      </section>
    );
  }
}

export default Layout;
