import React from "react";
import "./Hero.css";

const HeroImage = () => (
  <section className="banner full">
    <article className="visible top">
      <img src="images/slide01.jpg" alt="" />
      <div className="inner">
        <header>
          <p>Marvel Studios's</p>
          <h2>MARVEL</h2>
        </header>
      </div>
    </article>
  </section>
);

export default HeroImage;
