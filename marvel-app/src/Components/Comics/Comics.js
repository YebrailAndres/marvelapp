import React, { Component } from "react";
import LikeBtn from "../LikeButton/LikeBtn";
import './comics.css';

class Comics extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { title, image, date } = this.props;

    return (
      <div>
        <div className="box">
          <div className="image fit">
            <img src={`${image}.jpg`} alt="Comics" />
          </div>
          <div className="contentName">
            <header className="align-center">
              <p>{title}</p>
              <h2>{`Date: ${date}`}</h2>
              <LikeBtn />
            </header>
          </div>
        </div>
      </div>
    );
  }
}

export default Comics;
