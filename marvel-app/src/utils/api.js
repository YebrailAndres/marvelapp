import axios from "axios";

const BASE_URL = "http://gateway.marvel.com/v1/public/creators/32";
const APY_KEY = "b5dd158dd0e856443db7fb726fbc6bc9";
const HASH = "80182fcb24c6426319114b9e34eafed6";
const TS = "1";

export function getComics() {
  return axios.get(`${BASE_URL}/comics?ts=${TS}&apikey=${APY_KEY}&hash=${HASH}`);
}
